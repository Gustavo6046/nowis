import express from 'express';
import expressWs from 'express-ws';
import { WindowSystem } from 'nowis.core/src/system';
import { ProtocolCall } from 'nowis.core/src/protocol';
import { BaseWidget } from 'nowis.core/src/widget';



export default class NowisServer {
    public app: expressWs.Application;
    public ws: expressWs.Instance;

    protected systems: Set<WindowSystem> = new Set();
    protected endpointSystems: Map<string, WindowSystem> = new Map();


    constructor() {
        let app = express();
        this.ws = expressWs(app);
        this.app = app as any;
    }

    add(endpoint: string, system: WindowSystem) {
        this.systems.add(system);
        this.endpointSystems.set(endpoint, system);

        this.app.ws('/api/nowis/' + endpoint, (ws) => {
            if (!this.endpointSystems.has(endpoint)) ws.close(1, 'This system was deleted.');

            else {
                system.on('widget event', (event: string, at: BaseWidget, origin: BaseWidget, ...args: any[]) => {
                    ws.send(JSON.stringify({
                        type: "event",
                        event: event,
                        at: at.id,
                        origin: origin.id,
                        args: system.prep(args)
                    }));
                });

                let buffer = '';

                ws.on('message', (data) => {
                    buffer += data.toString('utf-8');

                    let lines = buffer.split('\n');
                    buffer = lines.pop()!;

                    lines.forEach((line) => {
                        let call: ProtocolCall;

                        try {
                            call = JSON.parse(line);
                        }

                        catch (e) {
                            console.error(e);
                            ws.close(2, 'Invalid JSON in call.');

                            return;
                        }

                        if (!(call.call && call.id && (call.type === 'perform' || call.type === 'query'))) {
                            ws.close(3, 'Invalid Nowis call format.');

                            return;
                        }

                        let resp = system.apply(call);

                        ws.send(JSON.stringify({
                            type: 'response',
                            response: resp
                        }) + '\n');
                    });
                });
            }
        });
    }

    remove(endpoint: string) {
        if (this.endpointSystems.has(endpoint)) {
            this.systems.delete(this.endpointSystems.get(endpoint)!);
            this.endpointSystems.delete(endpoint);
        }
    }

    listen(port: number, callback?: () => void) {
        return this.app.listen(port, callback);
    }
}