import NowisServer from ".";
import Nowis from "nowis.core";



let server = new NowisServer();

for (let i = 1; i <= (isNaN(+process.argv[3]) ? 1 : +process.argv[3]); i++) {
    let system = new Nowis.Systems.WindowSystem();
    server.add(''+i, system);
}

server.listen(isNaN(+process.argv[2]) ? 9595 : +process.argv[2], () => {
    console.log(`Listening on port ${isNaN(+process.argv[2]) ? 9595 : +process.argv[2]}.`);
});