"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_ws_1 = __importDefault(require("express-ws"));
var NowisServer = /** @class */ (function () {
    function NowisServer() {
        this.systems = new Set();
        this.endpointSystems = new Map();
        var app = express_1.default();
        this.ws = express_ws_1.default(app);
        this.app = app;
    }
    NowisServer.prototype.add = function (endpoint, system) {
        var _this = this;
        this.systems.add(system);
        this.endpointSystems.set(endpoint, system);
        this.app.ws('/api/nowis/' + endpoint, function (ws) {
            if (!_this.endpointSystems.has(endpoint))
                ws.close(1, 'This system was deleted.');
            else {
                var buffer_1 = '';
                ws.on('message', function (data) {
                    buffer_1 += data.toString('utf-8');
                    var lines = buffer_1.split('\n');
                    buffer_1 = lines.pop();
                    lines.forEach(function (line) {
                        console.log(lines);
                        var call;
                        try {
                            call = JSON.parse(line);
                        }
                        catch (e) {
                            console.error(e);
                            ws.close(2, 'Invalid JSON in call.');
                            return;
                        }
                        if (!(call.call && call.id && (call.type === 'perform' || call.type === 'query'))) {
                            ws.close(3, 'Invalid Nowis call format.');
                            return;
                        }
                        var resp = system.apply(call);
                        ws.send(JSON.stringify(resp) + '\n');
                    });
                });
            }
        });
    };
    NowisServer.prototype.remove = function (endpoint) {
        if (this.endpointSystems.has(endpoint)) {
            this.systems.delete(this.endpointSystems.get(endpoint));
            this.endpointSystems.delete(endpoint);
        }
    };
    NowisServer.prototype.listen = function (port, callback) {
        return this.app.listen(port, callback);
    };
    return NowisServer;
}());
exports.default = NowisServer;
//# sourceMappingURL=index.js.map