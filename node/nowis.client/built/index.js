"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ws_1 = __importDefault(require("ws"));
var v4_1 = __importDefault(require("uuid/v4"));
var NowisClient = /** @class */ (function () {
    function NowisClient(href) {
        var _this = this;
        this.href = href;
        this.waiting = new Map();
        this.ws = new ws_1.default(href);
        var buffer = '';
        this.ws.on('message', function (data) {
            buffer += data.toString('utf-8');
            var lines = buffer.split('\n');
            buffer = lines.slice(-1)[0];
            lines.slice(0, -1).forEach(function (l) {
                var response = JSON.parse(l);
                console.log(response);
                if (_this.waiting.has(response.callId))
                    _this.waiting.get(response.callId)(response);
            });
        });
    }
    NowisClient.prototype.query = function (query) {
        var _this = this;
        return new Promise(function (resolve) {
            var id = v4_1.default();
            _this.send({
                call: query,
                id: id,
                type: 'query'
            }).then(function (resp) {
                resolve(resp);
            });
        });
    };
    NowisClient.prototype.perform = function (performer) {
        var _this = this;
        return new Promise(function (resolve) {
            var id = v4_1.default();
            _this.send({
                call: performer,
                id: id,
                type: 'perform'
            }).then(function (resp) {
                resolve(resp);
            });
        });
    };
    NowisClient.prototype.send = function (call) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.waiting.set(call.id, function (resp) { return resolve(resp); });
            _this.ws.send(JSON.stringify(call) + '\n');
        });
    };
    return NowisClient;
}());
exports.default = NowisClient;
//# sourceMappingURL=index.js.map