import WebSocket from 'ws';
import { ProtocolCall, ProtocolResponse, ProtocolQuery, ProtocolPerformer } from 'nowis.core/src/protocol';
export default class NowisClient {
    href: string;
    ws: WebSocket;
    waiting: Map<string, (response: ProtocolResponse) => void>;
    constructor(href: string);
    query(query: ProtocolQuery): Promise<ProtocolResponse>;
    perform(performer: ProtocolPerformer): Promise<ProtocolResponse>;
    send(call: ProtocolCall): Promise<ProtocolResponse>;
}
