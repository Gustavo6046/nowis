import WebSocket from 'ws';
import uuidv4 from 'uuid/v4';
import { ProtocolCall, ProtocolResponse, ProtocolQuery, ProtocolPerformer } from 'nowis.core/src/protocol';



export default class NowisClient {
    public ws: WebSocket;
    public waiting: Map<string, (response: ProtocolResponse) => void> = new Map();

    constructor(public href: string) {
        this.ws = new WebSocket(href);

        let buffer = '';

        this.ws.on('message', (data) => {
            buffer += data.toString('utf-8');

            let lines = buffer.split('\n');
            buffer = lines.slice(-1)[0];

            lines.slice(0, -1).forEach((l) => {
                let response: ProtocolResponse = JSON.parse(l);
                console.log(response);

                if (this.waiting.has(response.callId))
                    this.waiting.get(response.callId)!(response);
            });
        });
    }

    query(query: ProtocolQuery): Promise<ProtocolResponse> {
        return new Promise((resolve) => {
            let id = uuidv4();

            this.send({
                call: query,
                id: id,
                type: 'query'
            }).then((resp: ProtocolResponse) => {
                resolve(resp);
            });
        });
    }

    perform(performer: ProtocolPerformer): Promise<ProtocolResponse> {
        return new Promise((resolve) => {
            let id = uuidv4();

            this.send({
                call: performer,
                id: id,
                type: 'perform'
            }).then((resp: ProtocolResponse) => {
                resolve(resp);
            });
        });
    }

    send(call: ProtocolCall): Promise<ProtocolResponse> {
        return new Promise((resolve) => {
            this.waiting.set(call.id, (resp) => resolve(resp));

            this.ws.send(JSON.stringify(call) + '\n');
        });
    }
}