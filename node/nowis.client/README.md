# Node.js Windowing System

Nowis is a suite of Node.js packages and clients for other languages (e.g. Python and Rust) that
enables windowing from pure JavaScript, as well as other languages, using a JavaScript-based window
server. There is much more that can be done with a dedicated window server. It also handles global
scope events (such as user input) and widget scope events (such as resizing).

This package contains very basic client network code, that is merely a foundation for other, larger
Nowis client projects.