import $ from 'jquery';
import { boundFlags, boundsSize, intersect, ltrb, printBounds, union, Vec, xy, XY, xyvec, moveBounds, boundsPos } from '../../src/common';
import DragHandler from '../../src/handlers/drag';
import MouseHandler from '../../src/handlers/mouse';
import WindowLimitHandler from '../../src/handlers/window-limits';
import { WindowSystem } from '../../src/system';
import { BaseWidget, Widget, MethodThis } from '../../src/widget';
import { WindowWidget } from '../../src/window';



let system = (window as any).system = new WindowSystem();

let drawBounds = (window as any).drawBounds = function drawBounds(canvas: HTMLCanvasElement) {
    const ctx = canvas.getContext('2d')!;

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    let order = 0;
    let maxDepth = 1;

    system.widgets.forEach((w) => {
        let d = w.depth();
        if (d + 1 > maxDepth) maxDepth = d + 1;
    });

    system.widgets.forEach((w) => {
        if (w instanceof WindowWidget) {
            ctx.strokeStyle = '#08FA';
            ctx.fillStyle = '#08FA';
        }

        else {
            if (w instanceof Widget && (w as any).$highlighted) {
                ctx.fillStyle = `#00F7`;
            }
            
            else {
                ctx.fillStyle = `#${(255 * w.depth() / maxDepth).toString(16).padStart(2, '0')}${(255 - 255 * w.depth() / maxDepth).toString(16).padStart(2, '0')}0080`;
            }

            ctx.strokeStyle = `#FFF7`;
        }   

        let bounds = w.getGlobalBounds();
        let bsize = boundsSize(bounds);
        let pad = w.padding();

        if (w instanceof WindowWidget) {
            ctx.lineWidth = 1;
            ctx.strokeRect(bounds.left, bounds.top, bsize.x, bsize.y);

            ctx.fillStyle = '#08F8';
            ctx.textAlign = 'center';

            ctx.fillText(w.name()!, bounds.left + bsize.x / 2, bounds.top + bsize.y / 2);
        }

        else if (w instanceof Widget) {
            ctx.fillRect(bounds.left, bounds.top, bsize.x, bsize.y);

            order++;
            ctx.fillStyle = '#FFF8';
            ctx.textAlign = 'center';

            ctx.fillText(w.name()!, bounds.left + bsize.x / 2, bounds.top + bsize.y / 2);
        }

        ctx.lineWidth = 2;
        ctx.strokeStyle = '#F30C'

        w.cells.forEach((cell) => {
            let b = moveBounds(cell.getBounds(), boundsPos(w.getGlobalBounds()));
            let pad = cell.content.padding();

            b.left     += pad.left;
            b.top      += pad.top;
            b.right    -= pad.left + pad.right;
            b.bottom   -= pad.top  + pad.bottom;

            let xs = b.right - b.left;
            let ys = b.bottom - b.top;

            ctx.fillStyle = '#AA08';

            ctx.strokeRect(b.left, b.top, xs, ys);
        });
    });
}

let el: HTMLCanvasElement = document.querySelector('#canvas')! as HTMLCanvasElement;
let w = (window as any).w = WindowWidget.make({ data: { title: 'My Window', pos: xy(20, 10), size: xy(400, 350), padding: ltrb(0, 15, 0, 0) }, name: '(w)' });
let a = (window as any).a = Widget.make(w, { data: { padding: ltrb(5, 5, 5, 5), layout: { cellPos: xy(0, 0), cellSize: xy(1, 1), sticky: boundFlags('nswe') } }, name: 'A' });
let b = (window as any).b = Widget.make(a, { data: { padding: ltrb(5, 5, 5, 5), layout: { cellPos: xy(0, 0), cellSize: xy(1, 1), sticky: boundFlags('nswe') }, minSize: xy(50, 50) }, name: 'B' });
let c = (window as any).c = Widget.make(b, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(0, 0), cellSize: xy(1, 1), sticky: boundFlags('we') } }, name: 'C' });
let d = (window as any).d = Widget.make(a, { data: { padding: ltrb(5, 5, 5, 5), layout: { cellPos: xy(1, 0), cellSize: xy(2, 1), sticky: boundFlags('nswe') } }, name: 'D' });
let e = (window as any).e = Widget.make(d, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(0, 0), cellSize: xy(2, 2), sticky: boundFlags('nswe') } }, name: 'E' });
let f = (window as any).f = Widget.make(d, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(2, 1), cellSize: xy(1, 3), sticky: boundFlags('nswe') } }, name: 'F' });
let g = (window as any).f = Widget.make(w, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(1, 0), cellSize: xy(1, 1), sticky: boundFlags('nswe'), shrink: true } }, name: 'G' });
let f1 = (window as any).f1 = Widget.make(f, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(0, 0), cellSize: xy(2, 1), sticky: boundFlags('nswe') } }, name: 'F1' });
let f2 = (window as any).f2 = Widget.make(f, { data: { padding: ltrb(5, 5, 5, 5), minSize: xy(25, 25), layout: { cellPos: xy(2, 1), cellSize: xy(1, 2), sticky: boundFlags('nswe') } }, name: 'F2' });

system.add(w);

$(document.getElementById('canvas')!).on('mousedown', (e) => {
    let offs = $(document.getElementById('canvas')!).offset()!;

    system.emitAt(xy(e.pageX - offs.left, e.pageY - offs.top), 'mouse down', xyvec(e.pageX - offs.left, e.pageY - offs.top));
});

$(document.getElementById('canvas')!).on('mouseup', (e) => {
    let offs = $(document.getElementById('canvas')!).offset()!;

    system.emitAt(xy(e.pageX - offs.left, e.pageY - offs.top), 'mouse up', xyvec(e.pageX - offs.left, e.pageY - offs.top));
});

$(document.getElementById('canvas')!).on('mousemove', (() => {
    let last: XY | null = null;

    return (e: JQueryMouseEventObject) => {
        let offs = $(document.getElementById('canvas')!).offset()!;
        let pos = xy(e.pageX - offs.left, e.pageY - offs.top);

        if (last) {
            let offset = new Vec(pos).sub(new Vec(last));
            system.emitAt(pos, 'mouse move', pos, offset);
        }

        last = pos;
    }
})());

system.registerAllWidgets(MouseHandler);

system.registerAllWindows(DragHandler);
system.registerAllWindows(WindowLimitHandler(function() { return ltrb(0, 0, el.width, el.height); }));

system.registerAllWidgets({
    listeners: {
        'child added': function (origin: BaseWidget) {
            if (this.$widget === origin) (this.$widget as any).$highlighted = false;
        },
 
        'mouse enter': function (origin: BaseWidget) {
            if (this.$widget === origin) (this.$widget as any).$highlighted = true;
            drawBounds(el);
        },

        'mouse exit': function (origin: BaseWidget) {
            (this.$widget as any).$highlighted = false;
            drawBounds(el);
        },
    }
});

w.on('window drag', () => drawBounds(el));
w.on('scroll', () => drawBounds(el));
$('#update').on('click', () => { w.update(); drawBounds(el) });
drawBounds(el);

(window as any).intersect   = intersect;
(window as any).union       = union;
(window as any).ltrb        = ltrb;
(window as any).printBounds = printBounds;