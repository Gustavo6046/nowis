/// <reference types="node" />
import { BoundFlags, Bounds, XY, XYFlags } from './common';
import { Grid, LayoutCell } from './layout';
import { AreaStyle } from './style';
import { WindowSystem } from './winsystem';
import { WindowWidget } from './window';
import { EventEmitter } from 'events';
export interface WidgetStringContent {
    type: 'string';
    data: string;
}
export interface WidgetBinaryContent {
    type: 'binary';
    data: Buffer;
}
export interface WidgetNullContent {
    type: 'null';
    data: null;
}
export declare type WidgetContent = WidgetStringContent | WidgetBinaryContent | WidgetNullContent;
export interface WidgetLayout {
    cellPos: XY;
    cellSize: XY;
    sticky: BoundFlags;
    shrink?: boolean;
}
export interface WidgetLayoutOptions {
    grow?: XYFlags;
    shrink?: XYFlags;
    weight?: XY;
}
export interface WidgetOptions {
    id?: string;
    name?: string;
    data?: any;
    activeSystems?: string[];
    components?: {
        [name: string]: any;
    };
}
export interface WidgetHandler {
    listeners?: {
        [event: string]: HandlerListener;
    };
    methods?: {
        [name: string]: HandlerMethod;
    };
    data?: (this: MethodThis) => {
        [name: string]: any;
    };
}
export declare type HandlerListener = (this: MethodThis, origin: BaseWidget, ...args: any[]) => boolean | void;
export declare type HandlerGlobalListener = (event: string, at: BaseWidget, origin: BaseWidget, ...args: any[]) => boolean | void;
export declare type HandlerMethod = ((this: MethodThis, ...args: any[]) => any);
export interface MethodThis {
    [name: string]: HandlerMethod | any;
    $widget: BaseWidget;
    $id: string;
    $methods: {
        [name: string]: HandlerMethod;
    };
    $listeners: {
        [name: string]: HandlerListener;
    };
}
export interface SystemMethodThis extends MethodThis {
    $origin: BaseWidget;
    $components: {
        [name: string]: any;
    };
}
export declare type HandlingSystemListeners = {
    [name: string]: ((this: SystemMethodThis, origin: BaseWidget, ...args: any[]) => void);
};
export declare class HandlingSystem extends EventEmitter {
    name: string;
    defs: HandlingSystemListeners;
    constructor(name: string, defs: HandlingSystemListeners);
    activateFor(widget: BaseWidget): void;
    deactivateFor(widget: BaseWidget): void;
    emit(event: string, from: BaseWidget, origin: BaseWidget, ...args: any[]): boolean;
}
export declare abstract class BaseWidget extends Grid {
    id: string;
    _name: string | null;
    parent?: BaseWidget;
    system?: WindowSystem;
    children: Map<string, BaseWidget>;
    data: any;
    hMethods: {
        [name: string]: HandlerMethod;
    };
    hListeners: {
        [name: string]: HandlerListener;
    };
    globalListeners: Set<HandlerGlobalListener>;
    components: Map<string, any>;
    activeSystems: Set<string>;
    methodThis: MethodThis;
    constructor(parent?: Widget | WindowWidget, data?: any);
    onAny(listener: HandlerGlobalListener): void;
    notOnAny(listener: HandlerGlobalListener): boolean;
    depth(): number;
    name(name?: string | null): string | null;
    boundMethods(): {
        [name: string]: (this: MethodThis, ...args: any[]) => any;
    };
    boundListeners(): {
        [name: string]: HandlerListener;
    };
    bindListener(list: HandlerListener): (origin: BaseWidget, ...args: any[]) => boolean | void;
    register(handler: WidgetHandler): void;
    abstract getGlobalBounds(): Bounds;
    emitUp(evt: string | symbol, origin: BaseWidget, ...args: any[]): boolean;
    emitUpFromHere(evt: string | symbol, ...args: any[]): boolean;
    emitDown(evt: string | symbol, origin: BaseWidget, ...args: any[]): boolean;
    emitDownFromHere(evt: string | symbol, ...args: any[]): boolean;
    emitOut(evt: string | symbol, origin: BaseWidget, ...args: any[]): boolean;
    emit(evt: string | symbol, ...args: [BaseWidget, ...any[]]): boolean;
    emitHere(evt: string | symbol, ...args: any[]): boolean;
    emitOutFromHere(evt: string | symbol, ...args: any[]): boolean;
    destroy(reason?: string | null): void;
    addChild(w: Widget): void;
    removeChild(w: BaseWidget): void;
}
export interface WidgetData {
    minSize?: XY;
    content: WidgetContent;
    style: AreaStyle;
    padding?: Bounds;
    layout: WidgetLayout;
}
export declare class Widget extends BaseWidget {
    cell?: LayoutCell;
    parent: Widget | WindowWidget;
    window: WindowWidget;
    layoutBounds: Bounds;
    data: WidgetData;
    minSizeX(): number;
    minSizeY(): number;
    larger(): boolean;
    growCell(): void;
    minSize(): XY;
    serialize(system?: WindowSystem): {
        '$wt': string;
        '$wi': string;
        '$wn': (name?: string | null | undefined) => string | null;
        '$wp': string;
        '$wv': any;
        '$wc': any;
        '$ws': string[];
    };
    static deserialize(sys: WindowSystem, data: any): Widget;
    inheritStyle(from?: AreaStyle): AreaStyle;
    destroy(reason?: string | null): void;
    static make(parent: Widget | WindowWidget, options: WidgetOptions): Widget;
    constructor(parent: Widget | WindowWidget, data?: any, id?: string, name?: string);
    initialBounds(): Bounds;
    cellResized(): void;
    getGlobalBounds(): Bounds;
    sizeX(): number;
    sizeY(): number;
    size(): XY;
    padding(): Bounds;
}
//# sourceMappingURL=widget.d.ts.map