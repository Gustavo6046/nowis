"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var drag_1 = __importDefault(require("./drag"));
var mouse_1 = __importDefault(require("./mouse"));
exports.default = {
    DragHandler: drag_1.default,
    MouseHandler: mouse_1.default
};
//# sourceMappingURL=index.js.map