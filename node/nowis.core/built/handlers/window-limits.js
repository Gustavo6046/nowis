"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var window_1 = require("../window");
function default_1(windowLimits) {
    return {
        listeners: {
            'window drag': function wlimit_onDrag(origin) {
                var limits = windowLimits.bind(this)(origin);
                if (this.$widget === origin && this.$widget instanceof window_1.WindowWidget && this.$widget.system && this.pos) {
                    var b = this.$widget.getGlobalBounds();
                    var moved = false;
                    if (b.left < limits.left) {
                        this.pos.x += limits.left - b.left;
                        moved = true;
                    }
                    if (b.top < limits.top) {
                        this.pos.y += limits.top - b.top;
                        moved = true;
                    }
                    if (b.right > limits.right) {
                        this.pos.x += limits.right - b.right;
                        moved = true;
                    }
                    if (b.bottom > limits.bottom) {
                        this.pos.y += limits.bottom - b.bottom;
                        moved = true;
                    }
                    if (moved) {
                        this.$widget.emitUpFromHere('window drag capped');
                    }
                }
            },
        }
    };
}
exports.default = default_1;
;
//# sourceMappingURL=window-limits.js.map