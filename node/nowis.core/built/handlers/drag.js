"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var window_1 = require("../window");
var common_1 = require("../common");
exports.default = {
    data: function () {
        return {
            dragging: false,
            draggable: true,
        };
    },
    listeners: {
        'mouse down': function drag_mouseDown(origin) {
            if (this.draggable && this.$widget === origin && origin instanceof window_1.WindowWidget)
                this.dragging = true;
        },
        'mouse up': function drag_mouseUp(origin) {
            if (this.$widget === origin && origin instanceof window_1.WindowWidget)
                this.dragging = false;
        },
        'mouse exit': function drag_mouseUp(origin) {
            if (this.$widget === origin && origin instanceof window_1.WindowWidget)
                this.dragging = false;
        },
        'mouse move': function drag_mouseMove(origin, newPos, offset) {
            if (this.dragging && this.$widget === origin) {
                if (this.$widget instanceof window_1.WindowWidget) {
                    this.pos = new common_1.Vec(this.pos).add(offset).xy();
                    this.$widget.emitDownFromHere('window drag', offset);
                }
            }
        }
    }
};
//# sourceMappingURL=drag.js.map