"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var window_1 = require("../window");
var widget_1 = require("../widget");
var winsystem_1 = require("../winsystem");
var style_1 = require("../style");
var common_1 = require("../common");
//
var w = new window_1.WindowWidget(undefined, { size: common_1.xy(200, 200) }, undefined, 'my window');
var a = new widget_1.Widget(w, { padding: { left: 0, top: 5, bottom: 5, right: 0 } }, undefined, 'my first widget');
var b = new widget_1.Widget(a, { padding: { left: 10, top: 5, bottom: 5, right: 10 } }, undefined, 'my other widget');
var c = new widget_1.Widget(b, { padding: { left: 15, top: 15, bottom: 15, right: 15 } }, undefined, 'my prettiest widget');
a.data.style.colors.bg = { r: 0, g: 0, b: 0 };
a.data.style.colors.text = { r: 1, g: 0, b: 0 };
b.data.style.colors.text = { r: 1, g: 1, b: 1 };
c.data.style.colors.bg = new style_1.LinearGradient(common_1.xy(0, 0), common_1.xy(0, 1), common_1.rgb(0.45, 0.45, 0.45), common_1.rgb(0.275, 0.275, 0.275));
//
var wSys = new winsystem_1.WindowSystem();
wSys.registerAllWidgets({
    listeners: {
        'hello': function (origin) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            console.log("Hello from " + origin.name() + "! " + args.join(' '));
        }
    }
});
wSys.add(w);
b.on('hello', function (origin) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    if (this !== origin)
        console.log("Tis, " + this.name() + ", was greeted by " + origin.name() + "!");
}.bind(b));
c.on('hello', function (origin) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    if (this !== origin)
        console.log("Tis, " + this.name() + ", was greeted by " + origin.name() + "!");
}.bind(c));
w.emitDownFromHere('hello', 'I am very glassen!');
a.emitOutFromHere('hello', 'I am first and foremost!');
c.emitUpFromHere('hello', 'I am the beauty of spring!');
//# sourceMappingURL=minimal.js.map