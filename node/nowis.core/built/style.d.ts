import { XY, Vec } from './common';
export interface Font {
    family: string;
    size: number;
    weight: number;
}
export declare abstract class Filler {
    abstract colorAt(relPos: Vec): Color;
}
export interface Color {
    r: number;
    g: number;
    b: number;
}
export declare class ColorFiller extends Filler {
    col: Color;
    constructor(col: Color);
    colorAt(relPos: Vec): Color;
}
export declare abstract class Gradient extends Filler {
    from: Color;
    to: Color;
    start: number;
    stop: number;
    constructor(from: Color, to: Color, start?: number, stop?: number);
    interpColor(alpha: number): Color;
}
export declare class LinearGradient extends Gradient {
    origin: XY;
    toward: XY;
    from: Color;
    to: Color;
    start: number;
    stop: number;
    constructor(origin: XY, toward: XY, from: Color, to: Color, start?: number, stop?: number);
    colorAt(relPos: Vec): Color;
}
export declare class RadialGradient extends Gradient {
    origin: XY;
    radius: number;
    from: Color;
    to: Color;
    start: number;
    stop: number;
    constructor(origin: XY, radius: number, from: Color, to: Color, start?: number, stop?: number);
    colorAt(relPos: Vec): Color;
}
export interface ColorStyle {
    bg?: Color | Filler;
    text?: Color | Filler;
}
export interface AreaStyle {
    colors: ColorStyle;
    font: Font;
    substyles: {
        active?: ColorStyle;
        selected?: ColorStyle;
        hover?: ColorStyle;
    };
}
export interface Style {
}
//# sourceMappingURL=style.d.ts.map