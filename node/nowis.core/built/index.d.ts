import * as Common from './common';
import * as Systems from './winsystem';
import * as Protocol from './protocol';
import * as Styling from './style';
import * as Widgets from './widget';
import * as Windows from './window';
declare const _default: {
    Common: typeof Common;
    Systems: typeof Systems;
    Protocol: typeof Protocol;
    Styling: typeof Styling;
    WIdgets: typeof Widgets;
    Windows: typeof Windows;
    Handlers: {
        DragHandler: Widgets.WidgetHandler;
        MouseHandler: Widgets.WidgetHandler;
    };
};
export default _default;
//# sourceMappingURL=index.d.ts.map