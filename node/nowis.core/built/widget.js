"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var v4_1 = __importDefault(require("uuid/v4"));
var common_1 = require("./common");
var layout_1 = require("./layout");
var window_1 = require("./window");
var events_1 = require("events");
var HandlingSystem = /** @class */ (function (_super) {
    __extends(HandlingSystem, _super);
    function HandlingSystem(name, defs) {
        var _this = _super.call(this) || this;
        _this.name = name;
        _this.defs = defs;
        return _this;
    }
    HandlingSystem.prototype.activateFor = function (widget) {
        widget.activeSystems.add(this.name);
    };
    HandlingSystem.prototype.deactivateFor = function (widget) {
        widget.activeSystems.delete(this.name);
    };
    HandlingSystem.prototype.emit = function (event, from, origin) {
        var args = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            args[_i - 3] = arguments[_i];
        }
        if (event in this.defs) {
            var smThis_1 = {
                $widget: from,
                $id: from.id,
                $methods: from.hMethods,
                $listeners: from.hListeners,
                $origin: origin,
                $components: from.components
            };
            Object.keys(from.hMethods).forEach(function (k) {
                Object.defineProperty(smThis_1, k, {
                    configurable: true,
                    get: function () { return from.boundMethods()[k]; }
                });
            });
            Object.keys(from.data).forEach(function (k) {
                Object.defineProperty(smThis_1, k, {
                    configurable: true,
                    set: function (x) { return from.data[k] = x; },
                    get: function () { return from.data[k]; }
                });
            });
            from.components.forEach(function (_, k) {
                //this[k] = this.$widget.data[k];
                Object.defineProperty(smThis_1, k, {
                    configurable: true,
                    set: function (x) { return from.components.set(k, x); },
                    get: function () { return from.components.get(k); }
                });
            });
            this.defs[event].bind(smThis_1).apply(void 0, __spreadArrays([origin], args));
        }
        return _super.prototype.emit.apply(this, __spreadArrays([event, from, origin], args));
    };
    return HandlingSystem;
}(events_1.EventEmitter));
exports.HandlingSystem = HandlingSystem;
var BaseWidget = /** @class */ (function (_super) {
    __extends(BaseWidget, _super);
    function BaseWidget(parent, data) {
        var _this = _super.call(this) || this;
        _this.globalListeners = new Set();
        _this.components = new Map();
        _this.activeSystems = new Set();
        _this.id = v4_1.default();
        _this._name = null;
        _this.children = new Map();
        if (data)
            _this.data = data;
        _this.hMethods = {};
        _this.hListeners = {};
        _this.methodThis = {
            $widget: _this,
            $methods: _this.hMethods,
            $id: _this.id,
            $listeners: _this.hListeners
        };
        Object.assign(_this.methodThis, _this.data);
        return _this;
    }
    BaseWidget.prototype.onAny = function (listener) {
        this.globalListeners.add(listener);
    };
    BaseWidget.prototype.notOnAny = function (listener) {
        return this.globalListeners.delete(listener);
    };
    BaseWidget.prototype.depth = function () {
        if (this.parent)
            return this.parent.depth() + 1;
        return 0;
    };
    BaseWidget.prototype.name = function (name) {
        if (name !== undefined) {
            this._name = name;
            this.methodThis.$name = name;
        }
        return this._name;
    };
    BaseWidget.prototype.boundMethods = function () {
        var _this = this;
        var bound = {};
        Object.keys(this.hMethods).forEach(function (k) {
            var m = _this.hMethods[k].bind(_this.methodThis);
            var f = function _handlerBound(origin) {
                var _this = this;
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
                Object.keys(this.$widget.hMethods).forEach(function (k) {
                    Object.defineProperty(_this, k, {
                        configurable: true,
                        get: function () { return _this.$widget.boundMethods()[k]; }
                    });
                });
                Object.keys(this.$widget.data).forEach(function (k) {
                    Object.defineProperty(_this, k, {
                        configurable: true,
                        set: function (x) { return _this.$widget.data[k] = x; },
                        get: function () { return _this.$widget.data[k]; }
                    });
                });
                this.$widget.components.forEach(function (_, k) {
                    Object.defineProperty(_this, k, {
                        configurable: true,
                        set: function (x) { return _this.$widget.components.set(k, x); },
                        get: function () { return _this.$widget.components.get(k); }
                    });
                });
                return m.apply(void 0, __spreadArrays([origin], args));
            };
            bound[k] = f.bind(_this.methodThis);
        });
        return bound;
    };
    BaseWidget.prototype.boundListeners = function () {
        var _this = this;
        var bound = {};
        Object.keys(this.hListeners).forEach(function (k) {
            bound[k] = _this.bindListener(_this.hListeners[k]);
        });
        return bound;
    };
    BaseWidget.prototype.bindListener = function (list) {
        var m = list.bind(this.methodThis);
        var f = function (origin) {
            var _this = this;
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            Object.keys(this.$widget.hMethods).forEach(function (k) {
                Object.defineProperty(_this, k, {
                    configurable: true,
                    get: function () { return _this.$widget.boundMethods()[k]; }
                });
            });
            Object.keys(this.$widget.data).forEach(function (k) {
                Object.defineProperty(_this, k, {
                    configurable: true,
                    set: function (x) { return _this.$widget.data[k] = x; },
                    get: function () { return _this.$widget.data[k]; }
                });
            });
            this.$widget.components.forEach(function (_, k) {
                Object.defineProperty(_this, k, {
                    configurable: true,
                    set: function (x) { return _this.$widget.components.set(k, x); },
                    get: function () { return _this.$widget.components.get(k); }
                });
            });
            var res = m.apply(void 0, __spreadArrays([origin], args));
        };
        return f.bind(this.methodThis);
    };
    BaseWidget.prototype.register = function (handler) {
        var _this = this;
        if (handler.data)
            Object.assign(this.data, handler.data.bind(this.methodThis)());
        Object.assign(this.hMethods, handler.methods || {});
        if (handler.listeners)
            Object.keys(handler.listeners).forEach(function (k) {
                var l = handler.listeners[k];
                if (_this.hListeners[k]) {
                    var al_1 = _this.hListeners[k].bind(_this.methodThis);
                    var bl_1 = _this.bindListener(l);
                    l = function __combined(origin) {
                        var args = [];
                        for (var _i = 1; _i < arguments.length; _i++) {
                            args[_i - 1] = arguments[_i];
                        }
                        al_1.apply(void 0, __spreadArrays([origin], args));
                        bl_1.apply(void 0, __spreadArrays([origin], args));
                    };
                }
                _this.hListeners[k] = l;
            });
    };
    BaseWidget.prototype.emitUp = function (evt, origin) {
        var _a;
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var res = this.emit.apply(this, __spreadArrays([evt, origin], args));
        if (this.parent)
            (_a = this.parent).emitUp.apply(_a, __spreadArrays([evt, origin], args));
        return res;
    };
    BaseWidget.prototype.emitUpFromHere = function (evt) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.emitUp.apply(this, __spreadArrays([evt, this], args));
    };
    BaseWidget.prototype.emitDown = function (evt, origin) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var res = this.emit.apply(this, __spreadArrays([evt, origin], args));
        this.children.forEach(function (c) {
            c.emitDown.apply(c, __spreadArrays([evt, origin], args));
        });
        return res;
    };
    BaseWidget.prototype.emitDownFromHere = function (evt) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.emitDown.apply(this, __spreadArrays([evt, this], args));
    };
    BaseWidget.prototype.emitOut = function (evt, origin) {
        var _a;
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var res = this.emit.apply(this, __spreadArrays([evt, origin], args));
        if (this.parent)
            (_a = this.parent).emitUp.apply(_a, __spreadArrays([evt, origin], args));
        this.children.forEach(function (c) {
            c.emitDown.apply(c, __spreadArrays([evt, origin], args));
        });
        return res;
    };
    BaseWidget.prototype.emit = function (evt) {
        var _this = this;
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.globalListeners.forEach(function (listener) {
            listener.apply(void 0, __spreadArrays([evt.toString(), _this], args));
        });
        if (this.system) {
            this.activeSystems.forEach(function (systemName) {
                var _a;
                if (_this.system.widgetSystems.has(systemName))
                    (_a = _this.system.widgetSystems.get(systemName)).emit.apply(_a, __spreadArrays([evt.toString(), _this], args));
            });
        }
        var hListeners = this.boundListeners();
        if (hListeners[evt.toString()])
            hListeners[evt.toString()].bind(this.methodThis).apply(void 0, __spreadArrays([this], args));
        return _super.prototype.emit.apply(this, __spreadArrays([evt], args));
    };
    BaseWidget.prototype.emitHere = function (evt) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.emit.apply(this, __spreadArrays([evt, this], args));
    };
    BaseWidget.prototype.emitOutFromHere = function (evt) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.emitOut.apply(this, __spreadArrays([evt, this], args));
    };
    BaseWidget.prototype.destroy = function (reason) {
        if (reason === void 0) { reason = null; }
        this.emitOutFromHere('destroy', reason);
        if (this.parent)
            this.parent.removeChild(this);
        this.children.forEach(function (c) {
            c.destroy();
        });
    };
    BaseWidget.prototype.addChild = function (w) {
        this.children.set(w.id, w);
        w.emitUpFromHere('added');
    };
    BaseWidget.prototype.removeChild = function (w) {
        this.children.delete(w.id);
    };
    return BaseWidget;
}(layout_1.Grid));
exports.BaseWidget = BaseWidget;
var Widget = /** @class */ (function (_super) {
    __extends(Widget, _super);
    function Widget(parent, data, id, name) {
        if (data === void 0) { data = {}; }
        var _this = _super.call(this, parent, {}) || this;
        _this.layoutBounds = { left: 0, top: 0, right: 1, bottom: 1 };
        _this.data = {
            padding: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
            },
            content: {
                type: 'null',
                data: null
            },
            style: {
                colors: {},
                substyles: {},
                font: {
                    family: 'Verdana',
                    size: 12,
                    weight: 500
                }
            },
            layout: {
                cellPos: common_1.xy(0, 0),
                cellSize: common_1.xy(1, 1),
                sticky: {
                    left: false,
                    right: false,
                    top: false,
                    bottom: false
                }
            }
        };
        var lt = _this.data.layout;
        Object.assign(lt, data.layout || {});
        data.layout = lt;
        Object.assign(_this.data, data);
        _this.parent = parent;
        if (id)
            _this.id = id;
        if (name)
            _this._name = name;
        if (parent instanceof window_1.WindowWidget)
            _this.window = parent;
        else
            _this.window = parent.window;
        if (parent) {
            _this.parent = parent;
            _this.parent.addChild(_this);
        }
        return _this;
    }
    Widget.prototype.minSizeX = function () {
        var m = Math.max(this.gridMinWidth(), 0);
        //console.log(this.name(), 'w 1', m);
        if (this.data.minSize) {
            m = Math.max(m, this.data.minSize.x); /*console.log(this.name(), 'w 2', m);*/
        }
        if (this.data.padding) {
            m += this.data.padding.left + this.data.padding.right; /*console.log(this.name(), 'w 4', m);*/
        }
        return m;
    };
    Widget.prototype.minSizeY = function () {
        var m = Math.max(this.gridMinHeight(), 0);
        if (this.data.minSize)
            m = Math.max(m, this.data.minSize.y);
        if (this.data.padding)
            m += this.data.padding.top + this.data.padding.bottom;
        return m;
    };
    Widget.prototype.larger = function () {
        if (!this.cell || !this.cell.bounds)
            return false;
        var sz = this.minSize();
        var bz = common_1.boundsSize(this.cell.bounds);
        if (sz.x > bz.x)
            return true;
        if (sz.y > bz.y)
            return true;
        return false;
    };
    Widget.prototype.growCell = function () {
        if (!this.cell || !this.cell.bounds)
            return;
        var sz = this.minSize();
        var bz = common_1.boundsSize(this.cell.bounds);
        if (sz.x > bz.x)
            this.cell.bounds.right = this.cell.bounds.left + sz.x;
        if (sz.y > bz.y)
            this.cell.bounds.bottom = this.cell.bounds.top + sz.y;
    };
    Widget.prototype.minSize = function () {
        return common_1.xy(this.minSizeX(), this.minSizeY());
    };
    Widget.prototype.serialize = function (system) {
        return {
            '$wt': 'widget',
            '$wi': this.id,
            '$wn': this.name,
            '$wp': this.parent.id,
            '$wv': (system || this.system).prep(this.data),
            '$wc': (system || this.system).prep(this.components.entries()),
            '$ws': Array.from(this.activeSystems),
        };
    };
    Widget.deserialize = function (sys, data) {
        return Widget.make(sys.widgets.get(data.$wp), {
            data: sys.unprep(data.$wv),
            id: data.$wi,
            name: data.$wn,
            components: new Map(sys.unprep(data.$wc)),
            activeSystems: data.$ws
        });
    };
    Widget.prototype.inheritStyle = function (from) {
        if (from === void 0) { from = this.parent.inheritStyle(); }
        var res = {
            colors: {},
            substyles: {
                selected: {},
                active: {},
                hover: {}
            },
            font: {
                family: 'Verdana',
                size: 12,
                weight: 500
            }
        };
        if (from) {
            Object.assign(res.colors, from.colors);
            if (from.substyles.selected)
                Object.assign(res.substyles.selected, from.substyles.selected);
            if (from.substyles.active)
                Object.assign(res.substyles.active, from.substyles.active);
            if (from.substyles.hover)
                Object.assign(res.substyles.hover, from.substyles.hover);
        }
        if (this.data.style.substyles.selected)
            Object.assign(res.substyles.selected, this.data.style.substyles.selected);
        if (this.data.style.substyles.active)
            Object.assign(res.substyles.active, this.data.style.substyles.active);
        if (this.data.style.substyles.hover)
            Object.assign(res.substyles.hover, this.data.style.substyles.hover);
        Object.assign(res.colors, this.data.style.colors);
        return res;
    };
    Widget.prototype.destroy = function (reason) {
        if (reason === void 0) { reason = null; }
        _super.prototype.destroy.call(this, reason);
        if (this.window && this.window.system) {
            this.window.system.widgets.delete(this.id);
        }
    };
    Widget.make = function (parent, options) {
        var res = new Widget(parent, {}, options.id, options.name);
        if (options.data) {
            if (options.data.minSize)
                res.data.minSize = options.data.minSize;
            if (options.data.content)
                res.data.content = options.data.content;
            if (options.data.padding)
                res.data.padding = options.data.padding;
            if (options.data.layout)
                Object.assign(res.data.layout, options.data.layout);
            if (options.data.style)
                Object.assign(res.data.style, options.data.style);
        }
        if (options.components)
            res.components = new Map(Object.entries(options.components));
        if (options.activeSystems)
            res.activeSystems = new Set(options.activeSystems);
        parent.addAsCell(res);
        return res;
    };
    Widget.prototype.initialBounds = function () {
        var is = this.minSize();
        if (!this.cell)
            return common_1.sizeBounds(is);
        var cb = this.cell.getBounds();
        var sizes = common_1.boundsSize(cb);
        var res = {
            left: cb.left + sizes.x / 2 - is.x / 2,
            top: cb.top + sizes.y / 2 - is.y / 2,
            right: cb.left + sizes.x / 2 + is.x / 2,
            bottom: cb.top + sizes.y / 2 + is.y / 2,
        };
        return res;
    };
    Widget.prototype.cellResized = function () {
        if (this.cell) {
            var cb = this.cell.getBounds();
            this.layoutBounds = this.initialBounds();
            if (this.data.layout.sticky.left)
                this.layoutBounds.left = Math.min(this.layoutBounds.left, cb.left);
            if (this.data.layout.sticky.top)
                this.layoutBounds.top = Math.min(this.layoutBounds.top, cb.top);
            if (this.data.layout.sticky.right)
                this.layoutBounds.right = Math.max(this.layoutBounds.right, cb.right);
            if (this.data.layout.sticky.bottom)
                this.layoutBounds.bottom = Math.max(this.layoutBounds.bottom, cb.bottom);
        }
    };
    Widget.prototype.getGlobalBounds = function () {
        var res = JSON.parse(JSON.stringify(this.layoutBounds));
        if (this.parent) {
            var pb = this.parent.getGlobalBounds();
            res = common_1.intersect(common_1.moveBounds(res, common_1.boundsPos(pb)), pb);
        }
        return res;
    };
    Widget.prototype.sizeX = function () {
        return this.layoutBounds.right - this.layoutBounds.left;
    };
    Widget.prototype.sizeY = function () {
        return this.layoutBounds.bottom - this.layoutBounds.top;
    };
    Widget.prototype.size = function () {
        return common_1.xy(this.sizeX(), this.sizeY());
    };
    Widget.prototype.padding = function () {
        if (this.data.padding) {
            return JSON.parse(JSON.stringify(this.data.padding));
        }
        return _super.prototype.padding.call(this);
    };
    return Widget;
}(BaseWidget));
exports.Widget = Widget;
//# sourceMappingURL=widget.js.map