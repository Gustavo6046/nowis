/// <reference types="node" />
import { EventEmitter } from "events";
import { BoundFlags, Bounds, XY } from "./common";
import { Widget } from "./widget";
export interface LayoutCellData {
    sticky: BoundFlags;
    pos: XY;
    size: XY;
}
export declare class LayoutCell {
    grid: Grid;
    content: Widget;
    layoutData: LayoutCellData;
    bounds?: Bounds;
    constructor(grid: Grid, content: Widget, layoutData: LayoutCellData);
    getBounds(): Bounds;
    getSize(): XY;
    isColumn(x: number): boolean;
    isRow(y: number): boolean;
    minSizeX(): number;
    minSizeY(): number;
    getCellBounds(): Bounds;
}
export declare class Grid extends EventEmitter {
    tiles: Map<string, LayoutCell>;
    cells: Set<LayoutCell>;
    cellBounds: Bounds;
    pixelBounds: Bounds;
    columns: Set<number>;
    rows: Set<number>;
    columnSizes: Map<number, number>;
    rowSizes: Map<number, number>;
    constructor();
    hashPos(x: number, y: number): string;
    cellAt(x: number, y: number): LayoutCell | undefined;
    allWidth(): number;
    allHeight(): number;
    columnSize(x: number, debug?: boolean): number;
    columnMinSize(x: number, debug?: boolean): number;
    rowSize(y: number): number;
    rowMinSize(y: number): number;
    addAsCell(widget: Widget): LayoutCell | undefined;
    remove(cell: LayoutCell): void;
    gridMinWidth(): number;
    gridMinHeight(): number;
    update(): void;
    padding(): Bounds;
    calculateCellSizes(): void;
    _update(): void;
}
//# sourceMappingURL=layout.d.ts.map