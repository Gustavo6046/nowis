"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert_1 = __importDefault(require("assert"));
var window_1 = require("../window");
var widget_1 = require("../widget");
var winsystem_1 = require("../winsystem");
describe('System', function () {
    describe('#emit', function () {
        it('should be able to define components on a widget', function () {
            var w = window_1.WindowWidget.make({
                name: 'w'
            });
            var a = widget_1.Widget.make(w, {
                name: 'a',
                components: {
                    foo: 0
                },
            });
            var sys = new winsystem_1.WindowSystem();
            sys.add(w);
            sys.add(a);
            var hsys = new widget_1.HandlingSystem('setter', {
                'setFoo': function (origin) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    console.log('- set foo');
                    this.foo = 1;
                }
            });
            sys.add(hsys);
            hsys.activateFor(a);
            a.emitHere('setFoo');
            assert_1.default.equal(a.components.get('foo'), 1);
        });
        it('should be able to define data on a widget', function () {
            var w = window_1.WindowWidget.make({
                name: 'w'
            });
            var a = widget_1.Widget.make(w, {
                name: 'a'
            });
            a.data.foo = 0;
            var sys = new winsystem_1.WindowSystem();
            sys.add(w);
            sys.add(a);
            var hsys = new widget_1.HandlingSystem('setter', {
                'setFoo': function (origin) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    console.log('- set foo');
                    this.foo = 1;
                }
            });
            sys.add(hsys);
            hsys.activateFor(a);
            a.emitHere('setFoo');
            assert_1.default.equal(a.data.foo, 1);
        });
        it('should be able to call methods on a widget', function () {
            var w = window_1.WindowWidget.make({
                name: 'w'
            });
            var a = widget_1.Widget.make(w, {
                name: 'a'
            });
            a.data.foo = 0;
            a.register({
                methods: {
                    localSetter: function () {
                        console.log('- set foo');
                        this.foo = 1;
                    }
                }
            });
            var sys = new winsystem_1.WindowSystem();
            sys.add(w);
            sys.add(a);
            var hsys = new widget_1.HandlingSystem('setter', {
                'setFoo': function (origin) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    console.log('- calling local setter');
                    this.localSetter();
                }
            });
            sys.add(hsys);
            hsys.activateFor(a);
            a.emitHere('setFoo');
            assert_1.default.equal(a.data.foo, 1);
        });
    });
});
//# sourceMappingURL=compsys.js.map