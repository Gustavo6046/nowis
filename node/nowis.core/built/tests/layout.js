"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert_1 = __importDefault(require("assert"));
var common_1 = require("../common");
var window_1 = require("../window");
var widget_1 = require("../widget");
describe('Widget', function () {
    describe('#minSize', function () {
        it('should be padding only for an empty widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window' });
            var empty = widget_1.Widget.make(w, { name: 'empty', data: { padding: common_1.ltrb(2, 4, 2, 4) } });
            assert_1.default.equal(empty.minSizeX(), 4);
            assert_1.default.equal(empty.minSizeY(), 8);
        });
        it('should be padding + minSize for an empty widget with minimum size', function () {
            var w = window_1.WindowWidget.make({ name: 'window' });
            var empty = widget_1.Widget.make(w, { name: 'empty', data: { padding: common_1.ltrb(2, 4, 2, 4), minSize: common_1.xy(20, 4) } });
            assert_1.default.equal(empty.minSizeX(), 24);
            assert_1.default.equal(empty.minSizeY(), 12);
        });
        it('should be padding + content for a non-empty widget with larger content than minSize', function () {
            var w = window_1.WindowWidget.make({ name: 'window' });
            var a = widget_1.Widget.make(w, { name: 'container', data: { padding: common_1.ltrb(10, 10, 10, 10), minSize: common_1.xy(20, 20) } });
            var b = widget_1.Widget.make(a, { name: 'inside', data: { minSize: common_1.xy(30, 30) } });
            assert_1.default.equal(a.minSizeX(), 50);
            assert_1.default.equal(a.minSizeY(), 50);
        });
        it('should be padding + minSize for a non-empty widget with smaller content than minSize', function () {
            var w = window_1.WindowWidget.make({ name: 'window' });
            var a = widget_1.Widget.make(w, { name: 'container', data: { padding: common_1.ltrb(10, 10, 10, 10), minSize: common_1.xy(20, 20) } });
            var b = widget_1.Widget.make(a, { name: 'inside', data: { minSize: common_1.xy(10, 10) } });
            assert_1.default.equal(a.minSizeX(), 40);
            assert_1.default.equal(a.minSizeY(), 40);
        });
    });
    describe('#getGlobalBounds', function () {
        it('should be the window\'s position and size for any window widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(2, 2), size: common_1.xy(28, 28) } });
            w.update();
            assert_1.default.equal(w.getGlobalBounds().left, 2);
            assert_1.default.equal(w.getGlobalBounds().top, 2);
            assert_1.default.equal(w.getGlobalBounds().right, 30);
            assert_1.default.equal(w.getGlobalBounds().bottom, 30);
        });
        it('should be the same as the window\'s, for a single sticky widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(2, 2), size: common_1.xy(28, 28) } });
            var a = widget_1.Widget.make(w, { data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) } } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 2);
            assert_1.default.equal(a.getGlobalBounds().top, 2);
            assert_1.default.equal(a.getGlobalBounds().right, 30);
            assert_1.default.equal(a.getGlobalBounds().bottom, 30);
        });
        it('should be the same as a centered minSize, for a single, non-sticky widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(0, 0), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { data: { minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10);
            assert_1.default.equal(a.getGlobalBounds().top, 10);
            assert_1.default.equal(a.getGlobalBounds().right, 20);
            assert_1.default.equal(a.getGlobalBounds().bottom, 20);
        });
        it('should be shifted, for a single widget in a shifted window', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { data: { minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10 + 10);
            assert_1.default.equal(a.getGlobalBounds().top, 10 + 10);
            assert_1.default.equal(a.getGlobalBounds().right, 20 + 10);
            assert_1.default.equal(a.getGlobalBounds().bottom, 20 + 10);
        });
        it('should be the same as a centered minSize, except for left, for a single, left-sticky widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(0, 0), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { data: { layout: { sticky: common_1.boundFlags('l'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 0);
            assert_1.default.equal(a.getGlobalBounds().top, 10);
            assert_1.default.equal(a.getGlobalBounds().right, 20);
            assert_1.default.equal(a.getGlobalBounds().bottom, 20);
        });
        it('should be half the width for a widget that shares its parent horizontally', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().top, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().right, 10 + 15);
            assert_1.default.equal(a.getGlobalBounds().bottom, 10 + 30);
            assert_1.default.equal(b.getGlobalBounds().left, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().top, 10 + 0);
            assert_1.default.equal(b.getGlobalBounds().right, 10 + 30);
            assert_1.default.equal(b.getGlobalBounds().bottom, 10 + 30);
        });
        it('should be half the height for a widget that shares its parent vertically', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().top, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().right, 10 + 30);
            assert_1.default.equal(a.getGlobalBounds().bottom, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().left, 10 + 0);
            assert_1.default.equal(b.getGlobalBounds().top, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().right, 10 + 30);
            assert_1.default.equal(b.getGlobalBounds().bottom, 10 + 30);
        });
        it('should be half the width and height for a widget that shares its parent diagonally', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().top, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().right, 10 + 15);
            assert_1.default.equal(a.getGlobalBounds().bottom, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().left, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().top, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().right, 10 + 30);
            assert_1.default.equal(b.getGlobalBounds().bottom, 10 + 30);
        });
        it('should skip empty rows and columns', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(30, 30) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(20, 20), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(a.getGlobalBounds().left, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().top, 10 + 0);
            assert_1.default.equal(a.getGlobalBounds().right, 10 + 15);
            assert_1.default.equal(a.getGlobalBounds().bottom, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().left, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().top, 10 + 15);
            assert_1.default.equal(b.getGlobalBounds().right, 10 + 30);
            assert_1.default.equal(b.getGlobalBounds().bottom, 10 + 30);
        });
        it('should inherit its position from widget to widget', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(40, 40) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var c = widget_1.Widget.make(b, { name: 'inner', data: { minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(c.getGlobalBounds().left, 10 + 25);
            assert_1.default.equal(c.getGlobalBounds().top, 10 + 25);
            assert_1.default.equal(c.getGlobalBounds().right, 10 + 35);
            assert_1.default.equal(c.getGlobalBounds().bottom, 10 + 35);
        });
        it('should inherit its position from widget to widget, and still be able to share space', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(80, 80) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var c = widget_1.Widget.make(b, { name: 'inner left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var d = widget_1.Widget.make(b, { name: 'inner right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(c.getGlobalBounds().left, 10 + 40);
            assert_1.default.equal(c.getGlobalBounds().top, 10 + 40);
            assert_1.default.equal(c.getGlobalBounds().right, 10 + 60);
            assert_1.default.equal(c.getGlobalBounds().bottom, 10 + 80);
            assert_1.default.equal(d.getGlobalBounds().left, 10 + 60);
            assert_1.default.equal(d.getGlobalBounds().top, 10 + 40);
            assert_1.default.equal(d.getGlobalBounds().right, 10 + 80);
            assert_1.default.equal(d.getGlobalBounds().bottom, 10 + 80);
        });
        it('should inherit its position from widget to widget, and still be able to exclude padding', function () {
            var w = window_1.WindowWidget.make({ name: 'window', data: { pos: common_1.xy(10, 10), size: common_1.xy(80, 80) } });
            var a = widget_1.Widget.make(w, { name: 'left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 0), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var b = widget_1.Widget.make(w, { name: 'right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10), padding: common_1.ltrb(5, 5, 5, 5) } });
            var c = widget_1.Widget.make(b, { name: 'inner left', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(0, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            var d = widget_1.Widget.make(b, { name: 'inner right', data: { layout: { sticky: common_1.boundFlags('nswe'), cellPos: common_1.xy(1, 1), cellSize: common_1.xy(1, 1) }, minSize: common_1.xy(10, 10) } });
            w.update();
            assert_1.default.equal(c.getGlobalBounds().left, 10 + 40 + 5);
            assert_1.default.equal(c.getGlobalBounds().top, 10 + 40 + 5);
            assert_1.default.equal(c.getGlobalBounds().right, 10 + 60);
            assert_1.default.equal(c.getGlobalBounds().bottom, 10 + 80 - 5);
            assert_1.default.equal(d.getGlobalBounds().left, 10 + 60);
            assert_1.default.equal(d.getGlobalBounds().top, 10 + 40 + 5);
            assert_1.default.equal(d.getGlobalBounds().right, 10 + 80 - 5);
            assert_1.default.equal(d.getGlobalBounds().bottom, 10 + 80 - 5);
        });
    });
});
//# sourceMappingURL=layout.js.map