"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Common = __importStar(require("./common"));
var Systems = __importStar(require("./winsystem"));
var Protocol = __importStar(require("./protocol"));
var Styling = __importStar(require("./style"));
var Widgets = __importStar(require("./widget"));
var Windows = __importStar(require("./window"));
var handlers_1 = __importDefault(require("./handlers"));
exports.default = {
    Common: Common,
    Systems: Systems,
    Protocol: Protocol,
    Styling: Styling,
    WIdgets: Widgets,
    Windows: Windows,
    Handlers: handlers_1.default
};
//# sourceMappingURL=index.js.map