"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("./common");
var Filler = /** @class */ (function () {
    function Filler() {
    }
    return Filler;
}());
exports.Filler = Filler;
var ColorFiller = /** @class */ (function (_super) {
    __extends(ColorFiller, _super);
    function ColorFiller(col) {
        var _this = _super.call(this) || this;
        _this.col = col;
        return _this;
    }
    ColorFiller.prototype.colorAt = function (relPos) {
        return this.col;
    };
    return ColorFiller;
}(Filler));
exports.ColorFiller = ColorFiller;
var Gradient = /** @class */ (function (_super) {
    __extends(Gradient, _super);
    function Gradient(from, to, start, stop) {
        if (start === void 0) { start = 0; }
        if (stop === void 0) { stop = 1; }
        var _this = _super.call(this) || this;
        _this.from = from;
        _this.to = to;
        _this.start = start;
        _this.stop = stop;
        return _this;
    }
    Gradient.prototype.interpColor = function (alpha) {
        alpha = this.start + alpha * (this.stop - this.start);
        return {
            r: (this.to.r - this.from.r) * alpha + this.from.r,
            g: (this.to.g - this.from.g) * alpha + this.from.g,
            b: (this.to.b - this.from.b) * alpha + this.from.b,
        };
    };
    return Gradient;
}(Filler));
exports.Gradient = Gradient;
var LinearGradient = /** @class */ (function (_super) {
    __extends(LinearGradient, _super);
    function LinearGradient(origin, toward, from, to, start, stop) {
        if (start === void 0) { start = 0; }
        if (stop === void 0) { stop = 1; }
        var _this = _super.call(this, from, to, start, stop) || this;
        _this.origin = origin;
        _this.toward = toward;
        _this.from = from;
        _this.to = to;
        _this.start = start;
        _this.stop = stop;
        return _this;
    }
    LinearGradient.prototype.colorAt = function (relPos) {
        var orig = new common_1.Vec(this.origin);
        var dir = new common_1.Vec(this.toward).sub(orig);
        var dsq = dir.sqsize();
        var offs = relPos.sub(orig);
        var dot = dir.dot(offs) / dsq;
        return this.interpColor(dot);
    };
    return LinearGradient;
}(Gradient));
exports.LinearGradient = LinearGradient;
var RadialGradient = /** @class */ (function (_super) {
    __extends(RadialGradient, _super);
    function RadialGradient(origin, radius, from, to, start, stop) {
        if (start === void 0) { start = 0; }
        if (stop === void 0) { stop = 1; }
        var _this = _super.call(this, from, to, start, stop) || this;
        _this.origin = origin;
        _this.radius = radius;
        _this.from = from;
        _this.to = to;
        _this.start = start;
        _this.stop = stop;
        return _this;
    }
    RadialGradient.prototype.colorAt = function (relPos) {
        var offs = relPos.sub(new common_1.Vec(this.origin));
        return this.interpColor(Math.min(1, Math.max(0, offs.size() / this.radius)));
    };
    return RadialGradient;
}(Gradient));
exports.RadialGradient = RadialGradient;
//# sourceMappingURL=style.js.map