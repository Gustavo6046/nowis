/// <reference types="node" />
import { WindowWidget } from './window';
import { BaseWidget, WidgetHandler, HandlingSystem } from './widget';
import { ColorFiller, LinearGradient, RadialGradient } from './style';
import { Vec, XY } from './common';
import * as Protocol from './protocol';
import { EventEmitter } from 'events';
export declare class WindowSystem extends EventEmitter {
    windows: Map<string, WindowWidget>;
    widgets: Map<string, BaseWidget>;
    handlers: WidgetHandler[];
    windowHandlers: WidgetHandler[];
    widgetSystems: Map<string, HandlingSystem>;
    prep(a: Vec | BaseWidget | ColorFiller | LinearGradient | RadialGradient | any): any;
    rawPrep(a: any): any;
    unprep(a: any): any;
    rawUnprep(a: any): any;
    add(w: WindowWidget | BaseWidget | HandlingSystem): void;
    apply(pcall: Protocol.ProtocolCall): Protocol.ProtocolResponse;
    registerAllWidgets(handler: WidgetHandler): void;
    registerAllWindows(handler: WidgetHandler): void;
    touching(pos: XY): BaseWidget[];
    isTouching(pos: XY, widget: BaseWidget): boolean;
    emitAt(pos: XY, evt: string | symbol, ...args: any[]): void;
}
//# sourceMappingURL=winsystem.d.ts.map