import { WindowWidget } from './window';
import { BaseWidget, WidgetHandler } from './widget';
import { ColorFiller, LinearGradient, RadialGradient } from './style';
import { Vec, XY } from './common';
import * as Protocol from './protocol';
export declare class WindowSystem {
    windows: Map<string, WindowWidget>;
    widgets: Map<string, BaseWidget>;
    handlers: WidgetHandler[];
    windowHandlers: WidgetHandler[];
    prep(a: Vec | BaseWidget | ColorFiller | LinearGradient | RadialGradient | any): any;
    rawPrep(a: any): any;
    unprep(a: any): any;
    rawUnprep(a: any): any;
    constructor();
    add(w: WindowWidget | BaseWidget): void;
    apply(pcall: Protocol.ProtocolCall): Protocol.ProtocolResponse;
    registerAllWidgets(handler: WidgetHandler): void;
    registerAllWindows(handler: WidgetHandler): void;
    touching(pos: XY): BaseWidget[];
    isTouching(pos: XY, widget: BaseWidget): boolean;
    emitAt(pos: XY, evt: string | symbol, ...args: any[]): void;
}
//# sourceMappingURL=system.d.ts.map