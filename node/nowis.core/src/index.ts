import * as Common from './common';
import * as Systems from './winsystem';
import * as Protocol from './protocol';
import * as Styling from './style';
import * as Widgets from './widget';
import * as Windows from './window';
import Handlers from './handlers';



export default {
    Common: Common,
    Systems: Systems,
    Protocol: Protocol,
    Styling: Styling,
    WIdgets: Widgets,
    Windows: Windows,
    Handlers: Handlers
};