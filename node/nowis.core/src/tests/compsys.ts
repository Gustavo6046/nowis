import assert from 'assert';
import { WindowWidget } from '../window';
import { HandlingSystem, Widget } from '../widget';
import { WindowSystem } from '../winsystem';


describe('System', () => {
    describe('#emit', () => {
        it('should be able to define components on a widget', () => {
            const w = WindowWidget.make({
                name: 'w'
            });

            const a = Widget.make(w, {
                name: 'a',
                components: {
                    foo: 0
                },
            });

            const sys = new WindowSystem();

            sys.add(w);
            sys.add(a);

            const hsys = new HandlingSystem('setter', {
                'setFoo': function(origin, ...args) {
                    console.log('- set foo');
                    this.foo = 1;
                }
            });

            sys.add(hsys);

            hsys.activateFor(a);

            a.emitHere('setFoo');

            assert.equal(a.components.get('foo'), 1);
        });

        it('should be able to define data on a widget', () => {
            const w = WindowWidget.make({
                name: 'w'
            });

            const a = Widget.make(w, {
                name: 'a'
            });
            (a.data as any).foo = 0;

            const sys = new WindowSystem();

            sys.add(w);
            sys.add(a);

            const hsys = new HandlingSystem('setter', {
                'setFoo': function(origin, ...args) {
                    console.log('- set foo');
                    this.foo = 1;
                }
            });

            sys.add(hsys);

            hsys.activateFor(a);

            a.emitHere('setFoo');

            assert.equal((a.data as any).foo, 1);
        });

        it('should be able to call methods on a widget', () => {
            const w = WindowWidget.make({
                name: 'w'
            });

            const a = Widget.make(w, {
                name: 'a'
            });
            (a.data as any).foo = 0;

            a.register({
                methods: {
                    localSetter: function() {
                        console.log('- set foo');
                        this.foo = 1;
                    }
                }
            })

            const sys = new WindowSystem();

            sys.add(w);
            sys.add(a);

            const hsys = new HandlingSystem('setter', {
                'setFoo': function(origin, ...args) {
                    console.log('- calling local setter');
                    this.localSetter();
                }
            });

            sys.add(hsys);

            hsys.activateFor(a);

            a.emitHere('setFoo');

            assert.equal((a.data as any).foo, 1);
        });
    });
});