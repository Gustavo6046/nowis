import { Bounds, sizeBounds, XY, xy } from './common';
import { AreaStyle } from './style';
import { WindowSystem } from './winsystem';
import { BaseWidget } from './widget';


export interface WindowData {
    title?: string;
    
    pos:    XY;
    size:   XY;
    clip?:  Bounds;

    style:    AreaStyle;
    padding?: Bounds;
}


export interface WindowOptions {
    id?:            string;
    name?:          string;
    system?:        WindowSystem;

    data?:          any;
    activeSystems?: string[];
    components?:    { [name: string]: any };
}


export class WindowWidget extends BaseWidget {
    public data: WindowData = {
        style: {
            colors: {
                bg: { r: 1, g: 1, b: 1 },
                text: { r: 0, g: 0, b: 0 }
            },
    
            substyles: {
                selected: {
                    bg: { r: 0, g: 0.5, b: 1 },
                    text: { r: 1, g: 1, b: 1 }
                }
            },

            font: {
                family: 'Verdana',
                size:   12,
                weight: 500,
            },
        },
        
        pos: {
            x: 0,
            y: 0,
        },

        size: {
            x: 1,
            y: 1,
        }
    };

    serialize(system?: WindowSystem) {
        return {
            '$wt': 'window',
            '$wn': this.name,
            '$wi': this.id,
            '$wv': (system || this.system!).prep(this.data),
            '$wc': (system || this.system!).prep(this.components.entries()),
            '$ws': Array.from(this.activeSystems),
        };
    }

    static deserialize(sys: WindowSystem, data: any) {
        return WindowWidget.make({
            data: sys.unprep(data.$wv),
            id: data.$wi,
            name: data.$wn,
            components: new Map(sys.unprep(data.$wc)),
            activeSystems: data.$ws
        });
    }

    inheritStyle(): AreaStyle {
        return this.data.style;
    }

    destroy(reason: string | null = null) {
        super.destroy(reason);

        if (this.system) {
            this.system.widgets.delete(this.id);
            this.system.windows.delete(this.id);
        }
    }

    static make(opt: WindowOptions) {
        let res = new WindowWidget(undefined, {}, opt.id, opt.name || undefined);
        res.system = opt.system;

        if (opt.data) {
            if (opt.data.pos)     res.data.pos       = opt.data.pos;
            if (opt.data.size)    res.data.size      = opt.data.size;
            if (opt.data.title)   res.data.title     = opt.data.title;
            
            if (opt.data.padding) res.data.padding   = opt.data.padding;
            if (opt.data.style) Object.assign(res.data.style, opt.data.style || {});
            if (opt.data.clip)  Object.assign(res.data.clip,  opt.data.clip  || {});
        }

        if (opt.components)    res.components       = new Map(Object.entries(opt.components));
        if (opt.activeSystems) res.activeSystems    = new Set(opt.activeSystems);

        return res;
    }

    constructor(parent?: BaseWidget, data: any | null = null, id?: string, name?: string) {
        // Windows can't have parents.
        super();
        Object.assign(this.data, data);

        if (id)     this.id = id;
        if (name)   this._name = name;
    }

    // Layout Methods

    getGlobalBounds() {
        let pad = this.padding();

        return {
            left: this.data.pos.x + pad.left,
            top: this.data.pos.y + pad.top,
            right: this.data.pos.x + this.data.size.x + pad.left + pad.right,
            bottom: this.data.pos.y + this.data.size.y + pad.top + pad.bottom,
        }
    }

    padding(): Bounds {
        if (this.data.padding) {
            return JSON.parse(JSON.stringify(this.data.padding));
        }

        return super.padding();
    }
}