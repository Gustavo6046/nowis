import { WidgetHandler, BaseWidget, Widget, MethodThis } from '../widget';
import { WindowWidget } from '../window';
import { Vec, Bounds } from '../common';



export default function(windowLimits: (this: MethodThis, origin: BaseWidget) => Bounds): WidgetHandler {
    return {
        listeners: {
            'window drag': function wlimit_onDrag(origin: BaseWidget) {
                let limits = windowLimits.bind(this)(origin);

                if (this.$widget === origin && this.$widget instanceof WindowWidget && this.$widget.system && this.pos) {
                    let b = this.$widget.getGlobalBounds();
                    let moved = false;
    
                    if (b.left < limits.left) {
                        this.pos.x += limits.left - b.left;
                        moved = true;
                    }
    
                    if (b.top < limits.top) {
                        this.pos.y += limits.top - b.top;
                        moved = true;
                    }
    
                    if (b.right > limits.right) {
                        this.pos.x += limits.right - b.right;
                        moved = true;
                    }
    
                    if (b.bottom > limits.bottom) {
                        this.pos.y += limits.bottom - b.bottom;
                        moved = true;
                    }

                    if (moved) {
                        this.$widget.emitUpFromHere('window drag capped');
                    }
                }
            },
        }
    };
};