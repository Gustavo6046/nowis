import { WidgetHandler, BaseWidget, Widget } from '../widget';
import { WindowWidget } from '../window';
import { Vec } from '../common';



export default {
    data: function() {
        return { mouseInside: false }
    },
    
    listeners: {
        'mouse move': function mouse_mouseMove(origin: BaseWidget, newPos: Vec, offset: Vec) {
            if (this.$widget === origin) {
                if (!this.mouseInside) {
                    if (this.$widget instanceof Widget && this.$widget.window) {
                        this.$widget.window.emitDownFromHere('mouse exit');
                    }

                    else if (this.$widget instanceof WindowWidget && this.$widget.system) {
                        this.$widget.system.windows.forEach((c) => {
                            if (c !== this.$widget && (c.data as any).mouseInside) {
                                (c.data as any).mouseInside = false;
                                c.emitDownFromHere('mouse exit');
                            }
                        });
                    }

                    this.mouseInside = true;
                    this.$widget.emitDownFromHere('mouse enter');
                }
            }
        }
    }
} as WidgetHandler;