import { WindowWidget } from '../window';
import { Widget, WidgetHandler, BaseWidget } from '../widget';
import { WindowSystem } from '../winsystem';
import { LinearGradient } from '../style';
import { xy, rgb } from '../common';



//

let w = new WindowWidget(undefined, { size: xy(200, 200) }, undefined, 'my window');
let a = new Widget(w, { padding: { left: 0, top: 5, bottom: 5, right: 0 } }, undefined, 'my first widget');
let b = new Widget(a, { padding: { left: 10, top: 5, bottom: 5, right: 10 } }, undefined, 'my other widget');
let c = new Widget(b, { padding: { left: 15, top: 15, bottom: 15, right: 15 } }, undefined, 'my prettiest widget');

a.data.style.colors.bg = { r: 0, g: 0, b: 0 };
a.data.style.colors.text = { r: 1, g: 0, b: 0 };
b.data.style.colors.text = { r: 1, g: 1, b: 1 };
c.data.style.colors.bg = new LinearGradient(
    xy(0, 0),
    xy(0, 1),
    rgb(0.45, 0.45, 0.45),
    rgb(0.275, 0.275, 0.275)
);

//

let wSys: WindowSystem = new WindowSystem();

wSys.registerAllWidgets({
    listeners: {
        'hello': (origin, ...args: string[]) => {
            console.log(`Hello from ${origin.name()}! ${args.join(' ')}`);
        }
    }
});

wSys.add(w);

b.on('hello', function (this: Widget, origin: BaseWidget, ...args: string[]) {
    if (this !== origin) console.log(`Tis, ${this.name()}, was greeted by ${origin.name()}!`);
}.bind(b));

c.on('hello', function (this: Widget, origin: BaseWidget, ...args: string[]) {
    if (this !== origin) console.log(`Tis, ${this.name()}, was greeted by ${origin.name()}!`);
}.bind(c));

w.emitDownFromHere('hello', 'I am very glassen!');
a.emitOutFromHere('hello', 'I am first and foremost!');
c.emitUpFromHere('hello', 'I am the beauty of spring!');