# Node.js Windowing System

This is a suite of Node.js packages and clients for other languages (e.g. Python and Rust) that
enables windowing from pure JavaScript, as well as other languages, using a JavaScript-based window
server. There is much more that can be done with a dedicated window server. It also handles global
scope events (such as user input) and widget scope events (such as resizing).

However, this isn't concrete without a frontend client, that is, if there is no client connected to it
that can actually render the windows within the server. (See the **"How does this work?"** section below
for more!)

## How do I use this?

First, make sure you have Nowis installed.

```
yarn global add nowis.server
```

1. Start a Nowis server.

```
nowis.server --port 12012
```

2. Attach a frontend client.

    * For example, you might want to attach the Python 3 frontend Pywis. Install it, if you haven't already:
    ```
    python3 -m pip install Pywis
    ```

    * Then run:
    ```
    pywis 12012
    ```

3. Attach whatever you want to that server!

```
node rectangles.js --port 12012
```

## Woah! How does this work?

Nowis is merely a server that manages and keeps track of multiple windows, their metadata and their
contents. It can save and load their state using a special database.

![How Nowis works.](imgs/diagram-1.png)

## Legal stuff (again...) 

©2019 Gustavo Ramos Rehermann (Gustavo6046). This project and its source code are entirely available under the MIT License. See LICENSE for more info.  
For contact, please use either:

* the email address (`rehermann6046@gmail.com`);
* the Discord tag (`Gustavo6046#9009`);
* the Keybase username (`gustavo6042`).

--------

***Chuck Nowis was here!***